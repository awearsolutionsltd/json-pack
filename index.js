const unpackEntries = [ // ordered by second value.
  ["Pnt", "Point"],
  ["acy", "accuracy"],
  ["bcm", "basic_movement"],
  ["bmgf", "basic_movement_geofence"],
  ["bcu", "basic_usage"],
  ["chlg", "challenge"],
  ["cui", "claim_uid"],
  ["cld", "claimed"],
  ["crds", "coordinates"],
  ["cnts", "counters"],
  ["cat", "created_at"],
  ["dmt", "day_movement"],
  ["end", "end"],
  ["eloc", "event_location"],
  ["eltm", "event_ltime"],
  ["ettm", "event_tag_time"],
  ["etm", "event_time"],
  ["gfc", "geofence"],
  ["gmy", "geometry"],
  ["hhr", "happy_hour"],
  ["hhbm", "happy_hour_basic_movement"],
  ["hhh", "happy_hour_hotspot"],
  ["hhps", "happy_hour_points"],
  ["hst", "hotspot"],
  ["hsi", "hotspot_id"],
  ["htt", "hotspot_time"],
  ["hmps", "hourly_mpoints"],
  ["itl", "initial"],
  ["iti", "item_id"],
  ["lbp", "last_basic_point"],
  ["lhlt", "last_happy_hour_ltime"],
  ["lhss", "last_hotspots"],
  ["lpt", "last_point_at"],
  ["ltt", "last_time"],
  ["lvlt", "leave_ltime"],
  ["lvt", "leave_time"],
  ["lmlt", "left_morning_location_at"],
  ["loc", "location"],
  ["mnl", "manual"],
  ["mrks", "markers"],
  ["mat", "matched_at"],
  ["mts", "matching_states"],
  ["mvat", "moved_at"],
  ["mvd", "movement_detection"],
  ["nat", "neat_at"],
  ["opts", "options"],
  ["phs", "phase"],
  ["ptg", "pointing"],
  ["pts", "points"],
  ["pid", "product_id"],
  ["prg", "progress"],
  ["rat", "removed_at"],
  ["rev", "rev"],
  ["REV", "revision"],
  ["rwd", "reward"],
  ["spd", "speed"],
  ["strt", "start"],
  ["shss", "stationary_hotspots"],
  ["tid", "tag_id"],
  ["ttms", "tag_times"],
  ["tim", "time"],
  ["tlt", "total_time"],
  ["typ", "type"]
];

// Keys must be 3-4 letter/number strings
const unpackMap = new Map(unpackEntries);

const packMap = new Map(unpackEntries.map(entry => [entry[1], entry[0]]));

if ((unpackMap.size !== unpackEntries.length) || (packMap.size !== unpackEntries.length)) {
  throw new Error("json_pack mappings are not 1 to 1.");
}

const packingResultRe = /^\.*(\w\w\w\w?|\d\d\d\d\d\dT\d\d\d\d\d\d.*|epc[\+\-]\d+.*)$/;

function packString(s) {
  const newDatePrefixMatch = s.match(/^20(\d\d)\-(\d\d)\-(\d\dT\d\d)\:(\d\d)\:(\d\d.*)$/);
  if (newDatePrefixMatch) { // looks like a timestamp.
    return newDatePrefixMatch.slice(1).join('');
  }
  const oldDatePrefixMatch = s.match(/^(19\d\d\-\d\d\-\d\dT\d\d\:\d\d\:\d\d)([^\d].*)?$/);
  if (oldDatePrefixMatch) {
    const secondsSinceEpoc = Math.round(new Date(`${oldDatePrefixMatch[1]}Z`) * 1e-3);
    if (!isNaN(secondsSinceEpoc)) { // (-0).toString() gives '0'
      return `epc${(0 <= secondsSinceEpoc)?'+':''}${secondsSinceEpoc}${oldDatePrefixMatch[2] || ''}`;
    }
  }
  if (packMap.has(s)) {
    return packMap.get(s);
  }
  if (packingResultRe.test(s)) {
    // This is a dangerous string that may be the result of packing.
    return `.${s}`;
  }

  return s;
}

function unpackString(s) {
  if (unpackMap.has(s)) {
    return unpackMap.get(s);
  }
  const newDateMatch = s.match(/^(\d\d)(\d\d)(\d\dT\d\d)(\d\d)(\d\d.*)$/);
  if (newDateMatch) {
    return `20${newDateMatch[1]}-${newDateMatch[2]}-${newDateMatch[3]}:${newDateMatch[4]}:${newDateMatch[5]}`;
  }
  const oldDateMatch = s.match(/^epc([\+\-]\d+)([^\d].*)?$/);
  if (oldDateMatch) {
    return (new Date(oldDateMatch[1] * 1e3)).toJSON().slice(0, -5) + oldDateMatch[2];
  }
  if ((s[0] === '.') && (packingResultRe.test(s.slice(1)))) {
    return s.slice(1);
  }
  return s;
}

let packStamp;
let packStampRe;
function generatePackStamp() {
  packStamp = `replace_${((Math.random() || 1) * 1e32).toString(36)}_${((Math.random() || 1) * 1e32).toString(36).slice(2)}_`.replace(/\W/g, '');
  packStampRe = new RegExp(`"${packStamp}([^"]+)"`, 'g');
}
generatePackStamp();

const shouldBePackedKeys = new Set();

function packObject(obj) {
  if ('string' === typeof obj) {
    return packString(obj);
  }
  if (Array.isArray(obj)) {
    return obj.map(packObject);
  }
  if (obj && ('object' === typeof obj)) { // non-null
    const newObj = {};
    Object.keys(obj).forEach(key => {
      const packedKey = packString(key);
      if ((packedKey === key) && (5 < key.length) && (key.length < 100) && (!shouldBePackedKeys.has(key)) && (shouldBePackedKeys.size < 1000) && (!/^[0-9a-fA-F]+$/.test(key)) && (/^[0-9a-zA-Z_\-]+$/.test(key)) && (/[a-zA-Z]{4}/.test(key))) {
        // warnings are logged only for object keys. Packed values may contain user-inputs.
        console.warn(`json-pack: key ${key} maybe should be packed`);
        shouldBePackedKeys.add(key);
        if (shouldBePackedKeys.size === 1000) {
          console.warn("json-pack: reached 1000 keys that should be packed - stopping logs");
        }
      }
      newObj[packedKey] = packObject(obj[key]);
    });
    return newObj;
  }
  if ('number' === typeof obj) {
    const match = JSON.stringify(obj).match(/^([\-]?\d+)\.(\d{9,})$/);
    if (match) { // it's a float with lot of fraction digits.
      return packStamp + Buffer.from(`${match[1]}.${match[2].slice(0, 8).replace(/0+$/, '') || '0'}`).toString('base64');
    }
  }
  return obj;
}

exports.pack = function(obj) {
  const stringifiedObj = JSON.stringify(obj) || 'null';
  while (0 <= stringifiedObj.indexOf(packStamp)) { // very very unlikely to happen.
    generatePackStamp();
  }
  obj = JSON.parse(stringifiedObj); // convert to a simple json-object
  return JSON.stringify(packObject(obj)).replace(packStampRe, function(_full, encoded) {
    return Buffer.from(encoded, 'base64').toString();
  });
};

function unpackObject(obj) {
  if ('string' === typeof obj) {
    return unpackString(obj);
  }
  if (Array.isArray(obj)) {
    return obj.map(unpackObject);
  }
  if (obj && ('object' === typeof obj)) {
    const newObj = {};
    Object.keys(obj).forEach(key => {
      newObj[unpackString(key)] = unpackObject(obj[key]);
    });
    return newObj;
  }
  return obj;
}

exports.unpack = function(s) {
  if ('undefined' === typeof s) { // safety
    return null; // unpacks to a json-stringifiable value.
  }
  return unpackObject(JSON.parse(s)); // JSON.parse(null) works: null is converted to 'null'.
};
